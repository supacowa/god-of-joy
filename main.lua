--[[
    This game is about you running a swinger club.
    Your objective is to bring joy of any kind to your visitors and making a lot money.
    Purchase toys, upgrade your invetory, offer new exiting services, enjoy it yourself and become the industry leader.

    NOTE you must run this game with LÖVE2D 11.1 or later

#   possible names for the released game:

    - god of joy
    - joy of love
    - joy club

#   coding style and naming conventions:

    This is a reminder about how I decided to write my code and why certain things are named as they are.

    CLASSES:    file name is singular and lowercase-hyphen-separated
                class name is singular and CapitalCamelCase (exept for classes that work like functions, e.g. uvTexture(), sprite() - these are lowerCamelCase)
                namespace is mostly global and they get required in conf.lua
    
    METHODS     function- and method calls are lowerCamelCase
                they either are part of a class, being defined locally or they belong into the helper file
                namespace is mostly local (relatively to their current scope)

    TODO        <extend later>

    2018 (c) kontakt@herrsch.de
]]

function love.load()
    -- TODO add simple in-game console for quick debug testing

    local window_width, window_height, window = love.window.getMode()
    local sx = window_width / window.minwidth
    local sy = window_height / window.minheight
    local window_zoom = (sx + sy) / 2 + 0.5

    player_camera = camera(0, 0, camera.smooth.damped(.1), window_zoom)
    player_cursor = cursor(player_camera)

    --cat = sprite("test/cat.gif")

    local swizzle = "rgba"
    local components = "(%l)(%l)(%l)(%l)"
    local order = "%1%2%3%4"

    for i = 1, 4 do
        for j = 1, 3 do
            local offset = i + j
            --string.gsub(swizzle, ".+", ""
            
        end
    end

    --print(string.gsub(swizzle, components, order))
end




function love.draw()
    updateMainThreadQueue()
    player_camera:attach()
    -- rooms, inventar, toys, etc
    --cat:draw()
    player_cursor:draw()
    player_camera:detach()
end




function love.mousepressed(x, y, button, touch)
    if button == 1 then
        player_cursor.current_animation = "grab"
    end
end




function love.mousereleased(x, y, button, touch)
    if button == 1 then
        player_cursor.current_animation = "hand"
    end
end
