require "helper"
require "class"
require "vector"
require "camera"
require "color"
require "sprite"
require "player.cursor"

function love.conf(g)
    g.version = "11.1" -- min love2d version
    g.identity = "goj" -- save directory name
    g.console = false -- Windows only
    g.externalstorage = false -- save files (and read from the save directory) in external storage on Android
    g.accelerometerjoystick = false -- accelerometer on iOS and Android is exposed as joystick
    g.gammacorrect = false

    g.window.vsync = true
    g.window.msaa = 8
    g.window.display = 1 -- active display index
    g.window.highdpi = false -- retina

    g.window.icon = nil -- path
    g.window.title = "god of joy"
    g.window.x = nil
    g.window.y = nil
    g.window.width = 768
    g.window.height = 432
    g.window.minwidth = 256
    g.window.minheight = 144
    g.window.borderless = false
    g.window.resizable = true
    g.window.fullscreen = false
    g.window.fullscreentype = "desktop"
 
    -- TODO turn of unused modules when releasing build

    g.modules.video = true
    g.modules.audio = true
    g.modules.sound = true
    g.modules.window = true
    g.modules.image = true
    g.modules.event = true
    g.modules.system = true
    g.modules.thread = true
    g.modules.physics = true
    g.modules.math = true
    g.modules.timer = true
    g.modules.graphics = true
    g.modules.keyboard = true
    g.modules.joystick = true
    g.modules.mouse = true
    g.modules.touch = true
end
