-- These colors are sampled from TIC-80 fantasy console color palette

return {
    {20,  12,  28,  255}, -- black
    {68,  36,  52,  255}, -- dark red
    {48,  52,  109, 255}, -- dark blue
    {78,  74,  79,  255}, -- dark gray
    {133, 76,  48,  255}, -- brown
    {52,  101, 36,  255}, -- dark green
    {208, 70,  72,  255}, -- red
    {117, 113, 97,  255}, -- light gray
    {89,  125, 206, 255}, -- light blue
    {210, 125, 44,  255}, -- organge
    {133, 149, 161, 255}, -- metallic gray
    {109, 170, 44,  255}, -- light green
    {210, 170, 153, 255}, -- peach
    {109, 194, 202, 255}, -- cyan
    {218, 212, 94,  255}, -- yellow
    {222, 238, 214, 255}  -- white
}