-- Hide default hardware cursor and replace it by a custom software cursor
-- In this particullar game it is better to have a software based cursor, because it can express any mouse motion through sprite animations.
--
-- 2018 (c) kontakt@herrsch.de

cursor = class(sprite)

function cursor:init(camera_controller)
    sprite.init(self, {
        atlas = love.graphics.newImage("player/cursor.png"),
        tile_size = vec2(16, 16),
        animations = {
            hand = {{delay = 0, address = vec2(1, 1)}},
            grab = {{delay = 0, address = vec2(2, 1)}},
            pinch = {{delay = 0, address = vec2(3, 1)}},
            all = {
                {delay = .25, address = vec2(1, 1)},
                {delay = .25, address = vec2(2, 1)},
                {delay = .25, address = vec2(3, 1)}
            }
        },
        current_animation = "hand",
        loop = false
    })

    local position = vec2()
    local position_delta = vec2()
    self.position_delta = vec2()
    self.camera = camera_controller

    self.position_delta.x = get(function()
        return position_delta.x
    end)

    self.position_delta.y = get(function()
        return position_delta.y
    end)

    self.position_delta.x = set(function(new_x)
        position_delta.x = new_x
    end)

    self.position_delta.y = set(function(new_y)
        position_delta.y = new_y
    end)

    self.position.x = get(function()
        return position.x
    end)

    self.position.y = get(function()
        return position.y
    end)

    self.position.x = set(function(new_x)
        self.position_delta.x = self.position.x - new_x
        position.x = new_x
    end)

    self.position.y = set(function(new_y)
        self.position_delta.y = self.position.y - new_y
        position.y = new_y
    end)

    love.mouse.setVisible(false) -- hide hardware cursor
end

function cursor:draw(animation_speed)
    if self.camera then
        self.position.x, self.position.y = self.camera:mousePosition( -- get world mouse position
            0,
            0,
            love.graphics.getWidth(),
            love.graphics.getHeight()
        )
    end
    sprite.draw(self, animation_speed)
end
