-- Just a collection of useful functions
-- 2018 (c) kontakt@herrsch.de


-- Clamps a number to within a certain range [n, m]
function clamp(n, low, high)
    return math.min(math.max(low or 0, n), high or 1)
end




-- Linear interpolation between two numbers
function lerp(from, to, fraction)
    local f = clamp(fraction)
    return (1 - f) * from + f * to
end




function lerp2(from, to, fraction)
    return from + (to - from) * clamp(fraction)
end




-- Cosine interpolation between two numbers
function cerp(from, to, fraction)
    local f = (1 - math.cos(clamp(fraction) * math.pi)) * .5
    return from * (1 - f) + to * f
end




-- Map value from one range to another
function remapRange(val, a1, a2, b1, b2)
    return b1 + (val-a1) * (b2-b1) / (a2-a1)
end




-- Returns -1 or +1
-- math.random() can generate random values from -n to +n but there is always zero in between these ranges
-- If you just need a positive or negative multiplier then use this function to generate one
    --
function randomSign()
    return 2 * math.random(1, 2) - 3
end




-- Round number from float to nearest integer based on adjacent delimiter
function roundNumber(float, limit)
    local i, f = math.modf(float)
    return f < limit and math.floor(float) or math.ceil(float)
end




-- Generate 2^n number sequence
-- [start]1, 2, 4, 8, 16, 32, 64, 128, ...[count]
--
function sequencePower2(count, start)
    local i = math.max(start or 0, 0)
    local j = i + count - 1
    local sequence = {}
    for n = i, j, 1 do
        table.insert(sequence, 2^n)
    end
    return sequence
end




-- Compare given number to array of numbers and return the closest one
function nearestNumber(n, array)
    local curr = array[1]
    for i = 1, #array do
        if math.abs(n - array[i]) < math.abs(n - curr) then
            curr = array[i]
        end
    end
    return curr
end




-- Calculate closest 2^n number to value
function nearestPower2(value)
    return math.log(value) / math.log(2)
end




-- check if given value is a integer or float (lua number) value
function isString(v)
    return v and type(v) == "string"
end




-- check if given value is a integer or float (lua number) value
function isNumber(v)
    return v and type(v) == "number"
end




-- Check if given number is an integer (not a float)
function isInteger(n)
    return n and isNumber(n) and n == math.floor(n)
end




-- Check if given value is of type table
function isTable(value)
    return value and type(value) == "table"
end




-- Check if given value is a table an has x, y [, z, w] properties
-- Technically this could also be a class or a plain table but as long as we find the right keys, everything will be fine
--
function isVector(value)
    return ((isTable(value) or type(value) == "userdata") and value.x and value.y) and true or false
end




function isFunc(value)
    return value and type(value) == "function"
end




-- If a variable is nil then choose default-boolean otherwise take variables boolean value
-- NOTE `return bool or default` will not work in this case because it would use default on both nil and false but we want only override the nil!
--
function defaultBoolean(bool, default)
    return type(bool) == "nil" and default or bool
end




-- Get the position of a point on screen as a dynamic percentage value [0-1]
function pixelPositionToPercentage(point_x, point_y)
    local width, height = love.window.getMode()
    return point_x / width, point_y / height
end




-- Get the pixel position on screen from a percentage value [0-1]
function percentPositionToPixel(dyn_x, dyn_y)
    local width, height = love.window.getMode()
    return width * dyn_x, height * dyn_y
end




-- Return perpendicular distance from point p0 to line defined by p1 and p2
function perpendicularDistance(p0, p1, p2)
    if p1.x == p2.x then
        return math.abs(p0.x - p1.x)
    end
    local m = (p2.y - p1.y) / (p2.x - p1.x) -- slope
    local b = p1.y - m * p1.x -- offset
    local dist = math.abs(p0.y - m * p0.x - b)
    return dist / math.sqrt(m*m + 1)
end




-- Prettified output of tables
function printf(array, indent)
    if not indent then indent = "" end
    local names = {}
    for n, g in pairs(array) do
        table.insert(names, n)
    end
    table.sort(names)
    for i, n in pairs(names) do
        local v = array[n]
        if isTable(v) then
            if v == array then -- prevent infinite loop on self reference
                print(indent..tostring(n)..": <-")
            else
                print(indent..tostring(n)..":")
                printf(v, indent.."   ")
            end
        elseif type(v) == "function" then
            print(indent..tostring(n).."()")
        else
            print(indent..tostring(n)..": "..tostring(v))
        end
    end
end




-- Gather uv information about any rectangular region (set of tiles) on a texture
-- Get a sequence of all region-rects from i to j where each sub-region is a tile of width x height
-- The 'explicit'-flag returns only tiles enclosed by the overall region from i to j (skipping the appendices and in-betweens)
-- Regions are described by their index position on texture - reading from top left corner on texture, indices are: 1,2,3...n
-- i and j indices might also be passed as vec2(col, row) which is convenient when spritesheet dimensions grow over time and where sprite indices might shift
--
function uvTexture(texture, region_width, region_height, i, j, explicit)
    local texture_width, texture_height = texture:getDimensions()
    local cols = texture_width / region_width
    local rows = texture_height / region_height
    
    -- get sprite index from col and row
    local function getId(cell)
        assert(vec2.isVector(cell), "bad optional parameters: region limits [i-j] must be numbers or vectors")
        return (cell.y - 1) * cols + cell.x
    end
    
    -- get col and row from sprite index
    local function getCell(id)
        local rem = id % cols
        local col = (rem ~= 0 and rem or cols) - 1
        local row = math.ceil(id / cols) - 1
        return col, row
    end
    
    i = i and (isNumber(i) and i or getId(i)) or 1 -- be sure to deal always with number indices
    j = j and (isNumber(j) and j or getId(j)) or i
    
    local minCol, minRow = getCell(i)
    local maxCol, maxRow = getCell(j)
    local tiles = {}
    local region = {}
    
    -- collect all tiles enclosed by i and j
    for k = i, j do
        local col, row = getCell(k)
        local w = 1 / cols
        local h = 1 / rows
        local u = w * col
        local v = h * row
        
        if not explicit or (col >= minCol and col <= maxCol) then
            table.insert(tiles, {
                id = k, -- region rect index on spritesheet
                col = col + 1, -- example: tile at {col = 1, row = 1}
                row = row + 1, -- beginning at the top left corner of the spritesheet
                x = col * region_width, -- {x, y} is the lower left corner position of the tile at {col, row}
                y = row * region_height,
                width = region_width,
                height = region_height,
                uv = {
                    x1 = u,
                    y1 = v,
                    x2 = u + w,
                    y2 = v + h,
                    w = w,
                    h = h
                }
            })
        end
    end
    
    -- sort tiles by column and row in ascending order
    table.sort(tiles, function(curr, list)
        return curr.row == list.row and curr.col < list.col or curr.row < list.row
    end)
    
    -- describe the overall region-rect
    local region = {
        x = tiles[1].x,
        y = tiles[1].y,
        width = tiles[#tiles].x + tiles[#tiles].width - tiles[1].x,
        height = tiles[#tiles].y + tiles[#tiles].height - tiles[1].y,
        uv = {
            x1 = tiles[1].uv.x1,
            y1 = tiles[1].uv.y1,
            x2 = tiles[#tiles].uv.x2,
            y2 = tiles[#tiles].uv.y2,
            w = tiles[#tiles].uv.x2 - tiles[1].uv.x1,
            h = tiles[#tiles].uv.y2 - tiles[1].uv.y1
        }
    }
    
    return region, tiles
end




-- Return random point inside a circle
function randomPointInCircle(radius)
    local t = 2 * math.pi * math.random()
    local u = math.random() + math.random()
    local r = u > 1 and (2-u) or u
    return
        radius * r * math.cos(t),
        radius * r * math.sin(t)
end




-- Test point in polygon
-- poly must be a list of vec2 items
--
function pointInPoly(x, y, poly)
    local oddNodes = false
    local j = #poly
    
    for i = 1, j do
        if (poly[i].y < y and poly[j].y >= y or poly[j].y < y and poly[i].y >= y) and (poly[i].x <= x or poly[j].x <= x) then
            if poly[i].x + (y - poly[i].y) / (poly[j].y - poly[i].y) * (poly[j].x - poly[i].x) < x then
                oddNodes = not oddNodes
            end
        end
        j = i
    end

    return oddNodes
end




-- Determine pixel positions on straight vector
-- Can be used for A* search algorithm or pixelated line drawings
-- Pass in two points of a vector (basically a line)
-- and receive a list of pixel positions from that input,
-- plus width and height values that describe each segment as a rectangle.
-- e.g. {{x, y, width, height}, ...}
--
function bresenham(x1, y1, x2, y2)
    local ax = math.min(x1, x2)
    local ay = math.min(y1, y2)
    local bx = math.max(x1, x2)
    local by = math.max(y1, y2)
    local dx = bx - ax
    local dy = ay - by
    local err, e2 = dx + dx -- error value e_xy
    local segments = {} -- line chunks which can be combined and drawn with multiple rectangles

    while true do
        e2 = 2 * err
        if #segments > 0 and segments[#segments].y == ay then -- increase previous line segment width
            segments[#segments].width = segments[#segments].width + 1
        elseif #segments > 0 and segments[#segments].x == ax then -- increase previous line segment height
            segments[#segments].height = segments[#segments].height + 1
        else -- create new line
            table.insert(segments, {x = ax, y = ay, width = 1, height = 1}) -- image.set(x1, y1)
        end
        if ax == bx and ay == by then break end
        if e2 > dy then err = err + dy; ax = ax + 1 end -- e_xy + e_x > 0
        if e2 < dx then err = err + dx; ay = ay + 1 end -- e_xy + e_y < 0
    end
    
    return segments
end




-- Curve fitting algorithm
function ramerDouglasPeucker(vertices, epsilon)
    epsilon = epsilon or .1
    local dmax = 0
    local index = 0
    local simplified = {}
    
    -- find point at max distance
    for i = 3, #vertices do
        local d = perpendicularDistance(vertices[i], vertices[1], vertices[#vertices])
        if d > dmax then
            index = i
            dmax = d
        end
    end
    
    -- recursively simplify
    if dmax >= epsilon then
        local list1 = {}
        local list2 = {}
        
        for i = 1, index - 1 do table.insert(list1, vertices[i]) end
        for i = index, #vertices do table.insert(list2, vertices[i]) end
        
        local result1 = ramerDouglasPeucker(list1, epsilon)
        local result2 = ramerDouglasPeucker(list2, epsilon)
        
        for i = 1, #result1 - 1 do table.insert(simplified, result1[i]) end
        for i = 1, #result2 do table.insert(simplified, result2[i]) end
    else
        for i = 1, #vertices do table.insert(simplified, vertices[i]) end
    end
    
    return simplified
end




-- CRC32 a widely used hash / checksum algorithm
-- found on https://github.com/openresty/lua-nginx-module/blob/master/t/lib/CRC32.lua
-- 2007-2008 (c) Neil Richardson (nrich@iinet.net.au)
--
do
    local max = 2^32 -1
    local CRC32 = {
        0,79764919,159529838,222504665,319059676,
        398814059,445009330,507990021,638119352,
        583659535,797628118,726387553,890018660,
        835552979,1015980042,944750013,1276238704,
        1221641927,1167319070,1095957929,1595256236,
        1540665371,1452775106,1381403509,1780037320,
        1859660671,1671105958,1733955601,2031960084,
        2111593891,1889500026,1952343757,2552477408,
        2632100695,2443283854,2506133561,2334638140,
        2414271883,2191915858,2254759653,3190512472,
        3135915759,3081330742,3009969537,2905550212,
        2850959411,2762807018,2691435357,3560074640,
        3505614887,3719321342,3648080713,3342211916,
        3287746299,3467911202,3396681109,4063920168,
        4143685023,4223187782,4286162673,3779000052,
        3858754371,3904687514,3967668269,881225847,
        809987520,1023691545,969234094,662832811,
        591600412,771767749,717299826,311336399,
        374308984,453813921,533576470,25881363,
        88864420,134795389,214552010,2023205639,
        2086057648,1897238633,1976864222,1804852699,
        1867694188,1645340341,1724971778,1587496639,
        1516133128,1461550545,1406951526,1302016099,
        1230646740,1142491917,1087903418,2896545431,
        2825181984,2770861561,2716262478,3215044683,
        3143675388,3055782693,3001194130,2326604591,
        2389456536,2200899649,2280525302,2578013683,
        2640855108,2418763421,2498394922,3769900519,
        3832873040,3912640137,3992402750,4088425275,
        4151408268,4197601365,4277358050,3334271071,
        3263032808,3476998961,3422541446,3585640067,
        3514407732,3694837229,3640369242,1762451694,
        1842216281,1619975040,1682949687,2047383090,
        2127137669,1938468188,2001449195,1325665622,
        1271206113,1183200824,1111960463,1543535498,
        1489069629,1434599652,1363369299,622672798,
        568075817,748617968,677256519,907627842,
        853037301,1067152940,995781531,51762726,
        131386257,177728840,240578815,269590778,
        349224269,429104020,491947555,4046411278,
        4126034873,4172115296,4234965207,3794477266,
        3874110821,3953728444,4016571915,3609705398,
        3555108353,3735388376,3664026991,3290680682,
        3236090077,3449943556,3378572211,3174993278,
        3120533705,3032266256,2961025959,2923101090,
        2868635157,2813903052,2742672763,2604032198,
        2683796849,2461293480,2524268063,2284983834,
        2364738477,2175806836,2238787779,1569362073,
        1498123566,1409854455,1355396672,1317987909,
        1246755826,1192025387,1137557660,2072149281,
        2135122070,1912620623,1992383480,1753615357,
        1816598090,1627664531,1707420964,295390185,
        358241886,404320391,483945776,43990325,
        106832002,186451547,266083308,932423249,
        861060070,1041341759,986742920,613929101,
        542559546,756411363,701822548,3316196985,
        3244833742,3425377559,3370778784,3601682597,
        3530312978,3744426955,3689838204,3819031489,
        3881883254,3928223919,4007849240,4037393693,
        4100235434,4180117107,4259748804,2310601993,
        2373574846,2151335527,2231098320,2596047829,
        2659030626,2470359227,2550115596,2947551409,
        2876312838,2788305887,2733848168,3165939309,
        3094707162,3040238851,2985771188,
    }

    local function xor(a, b)
        local calc = 0    

        for i = 32, 0, -1 do
        local val = 2 ^ i
        local aa = false
        local bb = false

        if a == 0 then
            calc = calc + b
            break
        end

        if b == 0 then
            calc = calc + a
            break
        end

        if a >= val then
            aa = true
            a = a - val
        end

        if b >= val then
            bb = true
            b = b - val
        end

        if not (aa and bb) and (aa or bb) then
            calc = calc + val
        end
        end

        return calc
    end

    local function lshift(num, left)
        local res = num * (2 ^ left)
        return res % (2 ^ 32)
    end

    local function rshift(num, right)
        local res = num / (2 ^ right)
        return math.floor(res)
    end

    function crc32(str) -- hash a string
        local count = string.len(tostring(str))
        local crc = max
        
        local i = 1
        while count > 0 do
        local byte = string.byte(str, i)

        crc = xor(lshift(crc, 8), CRC32[xor(rshift(crc, 24), byte) + 1])

        i = i + 1
        count = count - 1
        end

        return crc
    end
end




-- Run scripted sequence of commands after each other
-- Call `exec(function(thread) ... end)` to queue a routine
-- You can also pass any previously defined function and arguments instead of the closure above, e.g. `exec(print, "waited")`
-- Use `exec(wait, 1)` to create a delay between queued routines
--
do
    local queue = {}

    -- Call this method on every frame to keep queue in sync with the main Lua thread
    function updateMainThreadQueue()
        if #queue > 0 then
            if coroutine.status(queue[1]) == "dead" then table.remove(queue, 1)
            else coroutine.resume(queue[1], queue[1]) end
        end
    end

    -- Use this method to queue a function call
    function execute(func, ...)
        local params = {...}
        local thread = function(self) func(self, unpack(params)) end
        table.insert(queue, coroutine.create(thread))
    end

    -- Use this method to pause execution of the queue
    function wait(self, seconds)
        while love.timer.getTime() <= time + seconds do
            if type(self) == "thread" then
                coroutine.yield()
            end
        end
    end
end
