-- Prior versions of LOVE2D (<0.11.0) used RGBA values in range of [0-255] but since then values must be passed in as normalized values in range of [0-1].
-- It is easier to work with standard [0-255] values, so this convenience class allows to work with both formats by providing methods to access any format.
-- Call without arguments for white, e.g. `white = color()`, which is equivalent to `color(255)` or `color(255, 255)`or even `color(255, 255, 255, 255)`
-- Call with one argument for gray value `black = color(0)`
-- Create gray/alpha combo with two arguments, e.g. `transparent_gray = color(127, 127)`
--
-- (c) 2018 kontakt@herrsch.de

rgba = class()

function rgba:init(...)
    -- set missing parameters to defaults
    local varg = {...} -- r, g, b, a
    local max = 255
    local red = varg[1] or max
    local green = not varg[3] and red or (varg[2] or red)
    local blue = varg[3] or red
    local alpha = not varg[3] and (varg[4] or varg[2] or max) or (varg[4] or max)
    local normal = {}




    -- return all normalized components separatelly
    self.unpack = function()
        return red, green, blue, alpha
    end




    -- normalize given values on-the-fly and return them
    -- or simply return currently stored normalized components if no values were given
    self.normalized = function(this, r, g, b, a)
        if not r or not g or not b or not a then
            return this:unpack()
        end
    
        r = clamp(r, 0, max)
        g = clamp(g, 0, max)
        b = clamp(b, 0, max)
        a = clamp(a, 0, max)
    
        return -- normalize automatically whenever needed
            r > 1 and r / max or r,
            g > 1 and g / max or g,
            b > 1 and b / max or b,
            a > 1 and a / max or a
    end




    -- return components as normalized values between 0 and 1
    self.r = get(function() return red end)
    self.g = get(function() return green end)
    self.b = get(function() return blue end)
    self.a = get(function() return alpha end)

    -- assign components as normalized values between 0 and 1
    self.r = set(function(value) red = clamp(value, 0, 1) end)
    self.g = set(function(value) green = clamp(value, 0, 1) end)
    self.b = set(function(value) blue = clamp(value, 0, 1) end)
    self.a = set(function(value) alpha = clamp(value, 0, 1) end)




    -- return components as typical RGBA values between 0 and 255
    self.R = get(function() return red * max end)
    self.G = get(function() return green * max end)
    self.B = get(function() return blue * max end)
    self.A = get(function() return alpha * max end)

    -- assign components as typical RGBA values between 0 and 255 (they will be saved as normalized values internally)
    self.R = set(function(value)
        value = clamp(value, 0, max)
        red = value > 1 and value / max or value
    end)

    self.G = set(function(value)
        value = clamp(value, 0, max)
        green = value > 1 and value / max or value
    end)

    self.B = set(function(value)
        value = clamp(value, 0, max)
        blue = value > 1 and value / max or value
    end)

    self.A = set(function(value)
        value = clamp(value, 0, max)
        alpha = value > 1 and value / max or value
    end)
    
    self.getRGBA = function(this)
        return this.R, this.G, this.B, this.A -- use getters
    end

    self.setRGBA = function(this, r, g, b, a)
        this.R, this.G, this.B, this.A = r, g, b, a -- use setters
        return this
    end




    -- return components in HEX representation
    self.getHex = function(this)
        local hexadecimal = "0X"

        for _, value in ipairs{this.R, this.G, this.B} do
            local hex = ""
            
            while value > 0 do
                local index = math.fmod(value, 16) + 1
                value = math.floor(value / 16)
                hex = string.sub("0123456789ABCDEF", index, index)..hex			
            end
            
            if string.len(hex) == 0 then
                hex = "00"
            elseif string.len(hex) == 1 then
                hex = "0"..hex
            end

            hexadecimal = hexadecimal..hex
        end

        return string.sub(hexadecimal, 3) -- without the 0X prefix
    end

    -- assign components in HEX format (they will be converted and normalized internally)
    self.setHex = function(this, rgb) -- a six digit ("%06X") without alpha component, e.g. #ee45af
        rgb = string.match(tostring(rgb), "[^#]+")
        this.R, this.G, this.B, this.A = tonumber(rgb:sub(1, 2), 16), tonumber(rgb:sub(3, 4), 16), tonumber(rgb:sub(5, 6), 16), max
        return this
    end




    do -- generate getters and setter for swizzle fields (lower + upper case)
        -- grba
        -- gbra
        -- gbar
        
        -- bgar
        -- bagr
        -- barg

        -- abrg
        -- arbg
        -- argb

        -- ragb
        -- rgab
        -- rgba -- back to where it was originally

        local swizzle = "rgba"
    end


    -- TODO add methods for mixing/adding/substracting colors and converting color spaces if needed


    return self:setRGBA(red, green, blue, alpha)
end

-- TODO add __tostring metamethod?

return rgba
