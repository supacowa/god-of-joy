-- Create a textured (and animated) mesh quad
-- with optional GIF support
--
-- 2018 (c) kontakt@herrsch.de

--[[
    @params
    {
        atlas: image
        tile_size: vec2
        sprite_size: vec2
        position: vec2
        angle: number [0-360] degrees
        pivot: vec2 [0-1]
        animaitons: {}
        current_animation: "string"
        fps: number
        loop: boolean
        tint_color: color()
    }

    EXAMPLE:
    {
        atlas = love.graphics.newImage("image.png"),
        tile_size = vec2(8, 8),
        sprite_size = vec2(32, 32),
        position = vec2(0, 0),
        angle = 0,
        pivot = vec2(0, 0),
        animations = {
            default = {
                {
                    delay = .25
                    address = vec2(1, 1)
                }
            }
        },
        current_animation = "default",
        fps = 24, -- fallback in case the delay of some animation frame is missing
        loop = true,
        tint_color = color(255),
        visible = true,
        paused = false
    }
]]

local gif = require "gif"
local checksum = crc32
sprite = class()


function sprite:init(properties)
    if isString(properties) and properties:match("%.gif$") then -- is path to a GIF file
        -- TODO if properties is a path to GIF then parse and convert to atlas before this class works with it
        -- keep atlas just in RAM; do not cache it into a file on disk
        -- large images should be not packed into atlas but rather kept as separate images in memory??? or will this be ok?

        -- TODO compare frames with some kind of hash to only cache differen frames instead of duplicates

        local image = gif(properties)
        local width, height = image.get_width_height()
        local params = image.get_file_parameters()
        local frames = params.number_of_images
        local loop = params.looped
        local color = rgba()
        local atlas = love.image.newImageData(width, height)

        repeat
            params = image.get_image_parameters()
            local frame = params.image_no
            local delay = params.delay_in_ms
            local grid = image.read_matrix()

            for row = 1, #grid do
                for col = 1, #grid[1] do
                    local pixel = grid[row][col]
                    if pixel == -1 then -- transparent color
                        pixel = color:setRGBA(255, 0)
                    else -- opaque color without alpha component in 0xRRGGBB format
                        pixel = color:setHex(("%06X"):format(color))
                    end
                end
            end
        until not image.next_image()

        image.close()
    else
        for property, value in pairs(properties) do
            self[property] = value -- copy constructor parameters
        end

        -- TODO remove fps and instead introduce a delay variable per each frame so that GIF and spritesheet have same infrastructure to wirk with

        -- supplement missing parameters with presets
        self.sprite_size = self.sprite_size or vec2(self.tile_size.x, self.tile_size.y)
        self.tint_color = self.tint_color or rgba()
        self.position = self.position or vec2()
        self.pivot = self.pivot or vec2()
        self.angle = self.angle or 0
        self.fps = self.fps or 24
        self.loop = defaultBoolean(self.loop, true)
        self.visible = defaultBoolean(self.visible, true)
        self.paused = defaultBoolean(self.paused, false)
    end

    -- TODO add shader and method for 'palette swapping' of colors

    -- create mesh for drawing a [textured] rectangle
    local w = self.tile_size.x
    local h = self.tile_size.y
    local face_triangles = {1, 2, 4, 4, 2, 3} -- sequence to draw face_vertices
    local face_vertices = { -- x, y, [u, v, r, g, b, a]
        {0, 0, 0, 0}, -- top left
        {w, 0, 1, 0}, -- top right
        {w, h, 1, 1}, -- bottom right
        {0, h, 0, 1}  -- bottom left
    }

    self.quad = love.graphics.newMesh(face_vertices, "triangles", "static")
    self.quad:setVertexMap(face_triangles)
    self.quad:setTexture(self.atlas)
    self.quad:setAttributeEnabled("VertexColor", false)
end




function sprite:draw(time_scale)
    if self.paused then
        time_scale = 0 -- infinite multiplier
    end

    -- immediately reset timer and frame when animation changes
    if not self.previous_animation
    or self.previous_animation ~= self.current_animation
    then
        self.previous_animation = self.current_animation
        self.current_frame = 1
        self.timer = nil
    end

    -- keep quad size up-to-date by translating vertices directly
    -- self.quad:setVertexAttribute(1, 1, 0, 0) -- edge remains always at zero
    self.quad:setVertexAttribute(2, 1, self.tile_size.x, 0)
    self.quad:setVertexAttribute(3, 1, self.tile_size.x, self.tile_size.y)
    self.quad:setVertexAttribute(4, 1, 0, self.tile_size.y)

    if self.atlas then
        self.atlas:setFilter("linear", "nearest")

        if isTable(self.animations)
        and self.current_animation
        and self.current_animation ~= ""
        and self.animations[self.current_animation]
        and (not self.timer or self.timer <= love.timer.getTime())
        then
            local anim = self.animations[self.current_animation]
            local id = self.current_frame
            local frm = anim[id]
            local uv = uvTexture(self.atlas, self.tile_size.x, self.tile_size.y, frm.address).uv
            local dt = frm.delay
            
            -- update quad uv-coordinates for next animation frame
            self.quad:setVertexAttribute(1, 2, uv.x1, uv.y1)
            self.quad:setVertexAttribute(2, 2, uv.x2, uv.y1)
            self.quad:setVertexAttribute(3, 2, uv.x2, uv.y2)
            self.quad:setVertexAttribute(4, 2, uv.x1, uv.y2)

            local delay = clamp(time_scale or 1) * (dt or (1 / self.fps)) -- inf at 0 time_scale
            self.timer = love.timer.getTime() + delay
            self.current_frame = anim[id + 1] and id + 1 or 1
            
            if id == #anim and not self.loop then
                self.current_frame = id -- pullback
            end
        end
    end

    if self.visible then
        -- TODO handle angle property to controll rotation of sprite

        love.graphics.push("all")
        love.graphics.translate(self.position.x - self.pivot.x * self.sprite_size.x, self.position.y - self.pivot.y * self.sprite_size.y)
        love.graphics.scale(self.sprite_size.x / self.tile_size.x, self.sprite_size.y / self.tile_size.y)
        love.graphics.setColor(self.tint_color:unpack())
        love.graphics.draw(self.quad)
        love.graphics.pop()
    end
end
