-- Move-, zoom- and rotatable camera with movement locking and smoothing.
-- Code found on https://github.com/vrld/hump/blob/master/camera.lua
-- and adjusted to work with my class system and helper functions.
-- Documentation at http://hump.readthedocs.io/en/latest/camera.html

local cos, sin = math.cos, math.sin
camera = class()
camera.smooth = {} -- movement interpolators (for camera locking/windowing)

function camera.smooth.none()
	return function(dx, dy) return dx, dy end
end

function camera.smooth.linear(speed)
	assert(isNumber(speed), "Invalid parameter: speed = "..tostring(speed))
	return function(dx, dy, s)
		-- normalize direction
		local d = math.sqrt(dx * dx + dy * dy)
		local dts = math.min((s or speed) * love.timer.getDelta(), d) -- prevent overshooting the goal
		if d > 0 then
			dx, dy = dx / d, dy / d
		end
		return dx * dts, dy * dts
	end
end

function camera.smooth.damped(stiffness)
	assert(isNumber(stiffness), "Invalid parameter: stiffness = "..tostring(stiffness))
	return function(dx, dy, s)
		local dts = love.timer.getDelta() * (s or stiffness)
		return dx * dts, dy * dts
	end
end


function camera:init(x, y, smoother, zoom, angle)
    self.x = x or love.graphics.getWidth()/2
    self.y = y or love.graphics.getHeight()/2
	self.zoom = zoom or 1
	self.angle = angle or 0
	self.smoother = smoother or camera.smooth.none() -- for locking see below
end

function camera:lookAt(x, y)
	self.x, self.y = x, y
	return self
end

function camera:move(dx, dy)
	self.x, self.y = self.x + dx, self.y + dy
	return self
end

function camera:position()
	return self.x, self.y
end

function camera:rotate(phi)
	self.angle = self.angle + phi
	return self
end

function camera:rotateTo(phi)
	self.angle = phi
	return self
end

function camera:zoom(mul)
	self.zoom = self.zoom * mul
	return self
end

function camera:zoomTo(zoom)
	self.zoom = zoom
	return self
end

function camera:attach(x, y, w, h, noclip)
    x = x or 0
    y = y or 0
    w = w or love.graphics.getWidth()
    h = h or love.graphics.getHeight()

    self._sx, self._sy, self._sw, self._sh = love.graphics.getScissor()

    local cx = x + w / 2
    local cy = y + h / 2

	if not noclip then
		love.graphics.setScissor(x, y, w, h)
	end

	love.graphics.push("all")
	love.graphics.translate(cx, cy)
	love.graphics.scale(self.zoom)
	love.graphics.rotate(self.angle)
	love.graphics.translate(-self.x, -self.y)
end

function camera:detach()
	love.graphics.pop()
	love.graphics.setScissor(self._sx, self._sy, self._sw, self._sh)
end

function camera:draw(x, y, w, h, noclip, func)
	self:attach(x, y, w, h, noclip)
	func()
	self:detach()
end

-- world coordinates to camera coordinates
function camera:cameraCoords(x, y, ox, oy, w, h)
    ox = ox or 0
    oy = oy or 0
    w = w or love.graphics.getWidth()
    h = h or love.graphics.getHeight()
	
    local c = cos(self.angle)
    local s = sin(self.angle)
    x = c * (x - self.x) - s * (y - self.y)
    y = s * (x - self.x) + c * (y - self.y)
    
    return
        x * self.zoom + w / 2 + ox,
        y * self.zoom + h / 2 + oy
end

-- camera coordinates to world coordinates
function camera:worldCoords(x, y, ox, oy, w, h)
    ox = ox or 0
    oy = oy or 0
    w = w or love.graphics.getWidth()
    h = h or love.graphics.getHeight()
	
	local c = cos(-self.angle)
    local s = sin(-self.angle)
	x = (x - w/2 - ox) / self.zoom
    y = (y - h/2 - oy) / self.zoom
    x = c * x - s * y
    y = s * x + c * y
    
    return x + self.x, y + self.y
end

function camera:mousePosition(ox, oy, w, h)
	local mx, my = love.mouse.getPosition()
	return self:worldCoords(mx, my, ox, oy, w, h)
end

-- camera scrolling utilities
function camera:lockX(x, smoother, ...)
	local dx, dy = (smoother or self.smoother)(x - self.x, self.y, ...)
	self.x = self.x + dx
	return self
end

function camera:lockY(y, smoother, ...)
	local dx, dy = (smoother or self.smoother)(self.x, y - self.y, ...)
	self.y = self.y + dy
	return self
end

function camera:lockPosition(x, y, smoother, ...)
	return self:move((smoother or self.smoother)(x - self.x, y - self.y, ...))
end

function camera:lockWindow(x, y, x_min, x_max, y_min, y_max, smoother, ...)
    -- figure out displacement in camera coordinates
    x, y = self:cameraCoords(x, y)
    local dx, dy = 0, 0
    
	if x < x_min then
		dx = x - x_min
	elseif x > x_max then
		dx = x - x_max
	end
	if y < y_min then
		dy = y - y_min
	elseif y > y_max then
		dy = y - y_max
	end

	-- transform displacement to movement in world coordinates
	local c = cos(-self.angle)
    local s = sin(-self.angle)
	dx = (c * dx - s * dy) / self.zoom
    dy = (s * dx + c * dy) / self.zoom

	self:move((smoother or self.smoother)(dx, dy, ...))
end
